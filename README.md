# IT school


## lesson 1

[Add docker](https://gitlab.com/princeenvyanima/it-school/-/commit/5b7835fb21a955922dfd7799855f1a357e11e4b3)
## lesson 2

[Routing
Add gitignore](https://gitlab.com/princeenvyanima/it-school/-/commit/cccbabb6202cfaa3c66ad3b91df5b82060472829)

## lesson 3

[Templating
The HttpKernel Component](https://gitlab.com/princeenvyanima/it-school/-/commit/5cd80e2ee4eee3ed063c6365653baec8aaf00bab)

[Additionally The Separation of Concerns](https://gitlab.com/princeenvyanima/it-school/-/commit/f4ae6efadef4a28ac0b731fb393bd9933bba916b)

[Symfony initial deploy](https://gitlab.com/princeenvyanima/it-school/-/commit/a7e4eefe467fa3d6c60f46d96f4be33403ce84e0)
